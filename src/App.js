import {
    BrowserRouter,
    Routes,
    Route, 
    Switch
  } from "react-router-dom";
import Home from "./pages/home/Home";
import Theater from "./pages/theater-management/Theater";
import Show from "./pages/Show/Show";
import Seating from "./pages/Seating/Seating";
import React from 'react';
import Admin from "./pages/Admin/Admin";
import ListShow from "./components/AdminCrUD/ListShow";
import CreateShow from "./components/AdminCrUD/CreateShow";
import EditShow from "./components/AdminCrUD/EditShow";
import Help from "./pages/Help/Help";




function App() {
    return (
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<Home/>} />
            <Route path="/theater-management" element={<Theater/>} />
            <Route path="/theater-management/:id" element={<Theater/>} />
            <Route path="/Seat/:id" element={<Seating/>} />
            <Route path="admin" element={<Admin/>} />
            <Route path="admin/LShow" element={<ListShow/>} />
            <Route path="admin/CrShow" element={<CreateShow/>} />
            <Route path="admin/LShow/EShow/:id/edit" element={<EditShow/>} />
            <Route path="help" element={<Help/>} />
          </Routes> 
          
        </BrowserRouter> 
    );
  }
  
  export default App;