import React from 'react'
import './home.css'
import Navbar from '../../components/navbar/Navbar'
import Header from '../../components/header/Header'
import Features from '../../components/features/Features'

const Home = () => {
    return (
        <div>
            < Navbar />
            < Header />
            <div className="home-container">
                <Features />
            </div>
        </div>
)
}

export default Home