import React from 'react';
import {Link} from 'react-router-dom';
import Navbar from '../../components/navbar/Navbar';
import "./admin.css";

const Admin = () => {
    return (
        <div>
            < Navbar />
            <nav>
                <ul>
                    <li>
                        <Link to="/admin/LShow"> <div className='ad'>List Show</div></Link>
                    </li>
                    <li>
                        <Link to="/admin/CrShow"> <div className='ad'>Create Show</div></Link>
                    </li>
                    {/* <li>
                        <Link to="/admin/LShow/EShow/:id/edit"> Edit Show</Link>
                    </li> */}
                </ul>
            </nav>
        </div>
)
}

export default Admin