import React from 'react'
import Navbar from '../../components/navbar/Navbar'
import Header from '../../components/header/Header'

const Theater = () => {
    return (
        <div>
            <Navbar/>
            <Header type="theater-management"/>
        </div>
)
}

export default Theater