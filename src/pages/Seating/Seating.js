import React, { useEffect, useState } from "react";
import axios from "axios";
import { useNavigate, useParams } from "react-router-dom";
import Navbar from '../../components/navbar/Navbar';
import "./seating.css";


const Seat = () => {
    
    const navigate = useNavigate();
    const {id} = useParams();
    const orchestra = [];
    const [inputs, setInputs] = useState([]);

    const handleSubmit = (event) => {
        event.preventDefault();
        const selectedSeats = Array.from(document.getElementsByClassName('orchestra'));
        selectedSeats.forEach(selectedSeat => {
            if(selectedSeat.style.backgroundColor === "green")
            {
                axios.put(`http://localhost:8888/api/seat.php/${id}`, {'id' : selectedSeat.id}).then(function(response){
                    navigate('/');
                });
            }
        });
        
        
    }

    useEffect(() => {
        getSeats();
    }, []);

    const handleClick = (event) => {
        if (event.target.style.backgroundColor === "black"){
            event.target.style.backgroundColor = "green";
        } else if (event.target.style.backgroundColor = "red"){
            event.target.style.backgroundColor = "red";
        } else 
        event.target.style.backgroundColor = "black";

    };

    const getSeats = () => {
        axios.get(`http://localhost:8888/api/seat.php/${id}`).then(function(response){
            setInputs(response.data);
            
        });
    }
    console.log('hiihihihih',inputs);
    //
    for (let i = 1; i<101; i++) {   
        // //axios.post('http://localhost:8888/api/init_seat_table.php', {'id' : i}).then(function(response){
        //     console.log(response.data);
        // });
        if (inputs.length===0){
            continue;
        } else
        if (inputs[i-1].statut_show_1=== "taken"){
            var style = "red";
            console.log(i);
        } else
        style = "black";
        orchestra.push(
            <button className="orchestra" 
            id={i}
            onClick={handleClick}
            style={{backgroundColor: style}}>
            O{i} 
            </button> );
        
    }
    return (
        
        <div>
            < Navbar />
            <div className="seats" id='seats'>
            <div className='orch_seats'>{orchestra}</div>
            <button className="finish" onClick={handleSubmit}>Confirm payment ? </button>
            </div>  
        </div>
)
}

export default Seat