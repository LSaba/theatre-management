import React, { useEffect, useState } from "react";
import axios from "axios";
import './features.css'
import { Link } from "react-router-dom";

const Features = () => {
    const [shows, setShows] = useState([]);

    useEffect(() => {
        getShows();
    }, []);

    function getShows() {
      axios.get('http://localhost:8888/api/admin/LShow').then(function(response){
          setShows(response.data);
      });
    }

    const name = "blabla";

    // console.log("euuuuuuh"+getShows());
    
    
    // console.log("euuuuuh"+showy.name);

    return (
        <div className='shows'>
            {shows.map((show) => 
                <div className="theater">
                    <nav>
                        <ul>
                            <li>
                                <Link to={`/Seat/${show.id}`}> <div className="name"><h3>{show.name}</h3></div></Link>
                            </li>   
                        </ul>
                    </nav>
                    
                    {show.description}
                    <p>
                    <img className="img" src={show.img_url}></img> 
                    </p>
                    <p>
                    <button>8.8</button>
                    <span>Excellent</span>
                    </p>
                </div>
            )}
        </div>
    )
}

export default Features