import React from 'react'
import {Link} from 'react-router-dom';

import {
    faTicket,
    faQuestion,
    faPerson
  } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import "./navbar.css"

const Navbar = () => {
    return (
        <div className="nav-bar">
            <div className="nav-container"> 
                <span className="top"> 
                <nav>
                    <ul>
                        <li>
                            <Link to="/"> <div className="rialto">O Rialto !</div> </Link>
                        </li>
                    </ul>
                </nav>
                </span>
                {/* <div className="nav-items">
                    <button className="nav-button"> Home</button>
                    <button className="nav-button"> Plays</button>
                    <button className="nav-button"> Spotlight</button>
                </div> */}

            </div> 
            <div className="headerSearch">
                        <div className="iconquestion">
                                <FontAwesomeIcon icon={faQuestion} />
                                <nav>
                                    <ul>
                                        <li>
                                            <Link to="/help"> <div className="admin">Need help</div> </Link>
                                        </li>
                                    </ul>
                                </nav>
                        </div>   
                        <div className="iconticket">
                                <FontAwesomeIcon icon={faTicket} />
                                <nav>
                                    <ul>
                                        <li>
                                            <Link to="/Seat"> <div className="admin">Past Ticket</div> </Link>
                                        </li>
                                    </ul>
                                </nav>
                        </div> 
                        <div className="iconadmin">
                                <FontAwesomeIcon icon={faPerson}  />
                                <nav>
                                    <ul>
                                        <li>
                                            <Link to="/admin"> <div className="admin">Admin</div> </Link>
                                        </li>
                                    </ul>
                                </nav>
                        </div> 
                </div>
             
            </div>
)
}

export default Navbar
