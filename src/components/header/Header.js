import "./header.css";
import React from 'react';
import {
    faCalendarDays,
    faStreetView,
    faTheaterMasks
  } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { setOptions, localeFr } from '@mobiscroll/react';
import '@mobiscroll/react/dist/css/mobiscroll.min.css';
import { useState } from "react";
// import { format } from "date-fns";

setOptions({
    locale: localeFr,
    theme: 'ios',
    themeVariant: 'light'
});

const Header = () => {
        const [start, startRef] = React.useState(null);
        const [end, endstartRef] = React.useState(null);
        const [openDate, setOpenDate] = useState(false);
        
    return (
        <div className='title'>
                <h1 className='header-title'> Love a good show? </h1> <br/> 
                <p className='header-subtitle'>
                    You're in the place to be !
                </p>
                <div className="icons">
                        <FontAwesomeIcon icon={faStreetView} className="headerIcon" />
                            <select id="Types of seat ?" selected>
                                <option value="orch">Orchestra</option>
                                <option value="gtier">Grand Tier</option>
                                <option value="mezz">Mezzanine</option>
                                <option value="balc">Balcony</option>
                            </select>
                        <FontAwesomeIcon icon={faTheaterMasks} className="headerIcon" />
                            <select id="Choose your show" selected>
                                <option value="com">Comedy</option>
                                <option value="trag">Tragedy</option>
                                <option value="muthea">Musical Theatre</option>
                                <option value="drama">Drama</option>
                                <option value="tragcom">Tragicomedy</option>
                                <option value="op">Opera</option>
                            </select>
                        {/* <FontAwesomeIcon icon={faCalendarDays} className="headerIcon" /> */}
                        {/* <Datepicker
                                controls={['time']}
                                select="range"
                                type="text"
                                placeholder="When? Choose your time"
                                startInput={start}
                                endInput={end}
                                className="headerCursor"

                        /> */}
                          
                        <button className="headerBtn">Search</button>
                </div>   
        </div>

            
        
)
}

export default Header