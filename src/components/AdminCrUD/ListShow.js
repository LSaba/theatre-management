import React, { useEffect, useState } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import Navbar from '../../components/navbar/Navbar';

export default function ListShow() {
    const [shows, setShows] = useState([]);

    useEffect(() => {
        getShows();
    }, []);

    function getShows() {
        axios.get('http://localhost:8888/api/admin/LShow').then(function(response){
            console.log(response.data);
            setShows(response.data);
        });
    }

    const showy = shows.map((show) => <tr>
                                    <td>{show.name}</td>
                                    <td>{show.description}</td>
                                    <td>{show.img_url}</td>
                                    <td>
                                    <Link to={`EShow/${show.id}/edit`} style={{marginRight: "10px"}}>Edit</Link>
                                    <button>Delete</button></td>
                                    </tr>
                                    );

    return (
        <div>
            < Navbar />
            <h1>List Shows</h1>
            <table>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Image URL</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <ul>{showy}</ul> 
                </tbody>
            </table>
        </div>
   
    )
}