import React, { useEffect, useState } from "react";
import axios from "axios";
import { useNavigate, useParams } from "react-router-dom";
import Navbar from '../../components/navbar/Navbar';

export default function EditShow() {
    const navigate = useNavigate();

    const [inputs, setInputs] = useState([]);

    const {id} = useParams();

    useEffect(() => {
        getShows();
    }, []);

    function getShows() {
        axios.get(`http://localhost:8888/api/admin/LShow/EShow/${id}`).then(function(response){
            setInputs(response.data);
            console.log('euuuh',response.data);
        });
    }
    
    const handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        setInputs(values => ({...values, [name]: value}));
    }
    
    const handleSubmit = (event) => {
        event.preventDefault();
        axios.put(`http://localhost:8888/api/admin/LShow/EShow/${id}/edit`, inputs).then(function(response){
            console.log("123",id)
            navigate('/admin/LShow');
        });
    }
    return (
    <div>
        < Navbar />
        <h1>Edit Shows</h1>
        <form onSubmit={handleSubmit}>
            <table>
                <tbody>
                    <tr>
                        <th>
                            <label>Name : </label>            
                        </th>
                        <td>
                            <input value={inputs.name} type="text" name="name" onChange={handleChange}/>
                        </td>
                    </tr>
                    
                    <tr>
                        <th>
                            <label>Description : </label>
                        </th>
                        <td>
                            <input value={inputs.description} type="text" name="description" onChange={handleChange}/>
                        </td>
                    </tr>

                    <tr>
                        <th>
                            <label>Image : </label>
                        </th>
                        <td>
                            <input value={inputs.img_url} type="text" name="img_url" onChange={handleChange}/>
                        </td>
                    </tr>
                </tbody>
            </table>
            


            <button>Save</button>
        </form>
    </div>
    )
}