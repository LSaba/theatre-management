import React, { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import Navbar from '../../components/navbar/Navbar';

export default function CreateShow() {
    const navigate = useNavigate();

    const [inputs, setInputs] = useState([]);
    
    const handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        setInputs(values => ({...values, [name]: value}));
    }
    
    const handleSubmit = (event) => {
        event.preventDefault();
        axios.post('http://localhost:8888/api/admin/LShow', inputs).then(function(response){
            console.log(response.data);
            navigate('/admin/LShow');
        });
        
    }
    return (
    <div>
        < Navbar />
        <h1>holaaa Shows</h1>
        <form onSubmit={handleSubmit}>
            <table>
                <tbody>
                    <tr>
                        <th>
                            <label>Name : </label>            
                        </th>
                        <td>
                            <input type="text" name="name" onChange={handleChange}/>
                        </td>
                    </tr>
                    
                    <tr>
                        <th>
                            <label>Description : </label>
                        </th>
                        <td>
                            <input type="text" name="description" onChange={handleChange}/>
                        </td>
                    </tr>

                    <tr>
                        <th>
                            <label>Image : </label>
                        </th>
                        <td>
                            <input type="text" name="img_url" onChange={handleChange}/>
                        </td>
                    </tr>
                </tbody>
            </table>
            


            <button>Save</button>
        </form>
    </div>
    )
}