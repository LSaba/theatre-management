<?php 
error_reporting(E_ALL);
ini_set('display_errors', 1);
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Methods: *");

include 'dbConnect.php';
$objDb = new dbConnect;
$conn = $objDb->connect();
$method = $_SERVER['REQUEST_METHOD'];

switch($method) {
    case "PUT":
        $user = json_decode( file_get_contents('php://input') );
        $path = explode('/', $_SERVER['REQUEST_URI']);
        $sql = "UPDATE Seating SET statut_show_$path[3] = 'taken' ";
        $sql .= " WHERE id = $user->id;";
        $stmt = $conn->prepare($sql);
        if($stmt->execute()) {
            $response = ['status' => 1, 'message' => 'Record updated successfully.'];
        } else {
            $response = ['status' => 0, 'message' => 'Failed to update record.'];
        }        
        echo json_encode($response);
        break;

    case "GET":
        $user = json_decode( file_get_contents('php://input') );
        $path = explode('/', $_SERVER['REQUEST_URI']);
        $sql = "SELECT statut_show_1 FROM Seating";
        // $sql .= " WHERE id = $user->id;";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
	    $seats = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        echo json_encode($seats);
        break;
}
