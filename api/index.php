<?php 
error_reporting(E_ALL);
ini_set('display_errors', 1);
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Methods: *");

include 'dbConnect.php';
$objDb = new dbConnect;
$conn = $objDb->connect();

$method = $_SERVER['REQUEST_METHOD'];

switch($method) {
    case "GET":
        $sql = "SELECT * FROM Theater_show";
        $path = explode('/', $_SERVER['REQUEST_URI']);
        if(isset($path[5]) && is_numeric($path[5])) {
            $sql .= " WHERE id = '$path[5]'";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
	        // echo 'echooo'.$path[0].$path[1].$path[2].$path[3].$path[4].$path[5];
            // echo 'sqlllllll'.$sql;
            $shows = $stmt->fetch(PDO::FETCH_ASSOC);
        } else {
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $shows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        echo json_encode($shows);
        break;
    case "POST":
        $user = json_decode( file_get_contents('php://input') );
        $sql = "INSERT INTO Theater_show(name, description, img_url) VALUES('$user->name', '$user->description', '$user->img_url');";
        $stmt = $conn->prepare($sql);
        if($stmt->execute()) {
            $response = ['status' => 1, 'message' => 'Record created successfully.'];
        } else {
            $response = ['status' => 0, 'message' => 'Failed to create record.'];
        }
        echo json_encode($response);
        break;
    case "PUT":
        $user = json_decode( file_get_contents('php://input') );
        $sql = "UPDATE Theater_show SET name = '$user->name', description = '$user->description', img_url = '$user->img_url' WHERE id = '$user->id'";
        $stmt = $conn->prepare($sql);
        if($stmt->execute()) {
            $response = ['status' => 1, 'message' => 'Record updated successfully.'];
        } else {
            $response = ['status' => 0, 'message' => 'Failed to update record.'];
        }
        echo json_encode($response);
        break;
    }
