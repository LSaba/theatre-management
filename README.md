# Theatre management


## Getting started

- Download Nodejs following this link : https://nodejs.org/en/

- run 
    ```/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"```
in home terminal to install HomeBrew

- Next; run 
    ```brew install nodejs```

- Verification that node and npm versions are  "Node >= 14.0.0 and npm >= 5.6", by running 
    ```node -v```
    ```npm -v```

- Once, all this is verified, we can create our React app by running : 
    ```npx create-react-app admin-crud```

- Install "react-router-dom" in order to implement dynamic routing in a web app :
    ```npm install react-router-dom```

- Install axios to make it easy to send asynchronous HTTP requests to REST endpoints and perform CRUD operations : 
    ```npm install axios```





Once, all this is installed, we can run ```npm start``` that would open the app on http://localhost:3000/ .
Just make sure that api is on the localhost:8888 linked to Mamp, in order to connect to the database it's better to create a DB called `rialto-crud` and two tables `Theater_show` and `Seating`,


`Theater_show` will have 4 columns id serial, name varchar, description varchar, img_url varchar.


The app allows an admin to create a show and add it to the database and show it to users, also it allows editing and deleting a show from database and list show and front page of the app.

Once a show is added we could access the seating page that allows users to select 1 or more seats they turn green, the seating is once again connected to the database, so when confirming the purchase, and relorading the seating plan the seats become red which means that they could not be taken another time. 

This must work for all shows and each time we add a show we just need to ```ALTER TABLE Seatign ADD COLUMN statut_show_${id}``` in order to add a column that will manage the statut of each seating of the plan.

Unfortunately, I didn't have much time to upload a 'cancel ticket' interface but I would have proceeded by sending a unique id when confirming the purchase when buying a ticket so I would use a put method once again to update the statut from "taken" to "untaken" for user. 
I also wouldn't uploaded 'covid' situation but I would have added a bool column when creating a show called covid that when it's true would delete all pair seats. 
